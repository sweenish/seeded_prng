#ifndef THIS___RANDOM_HPP
#define THIS___RANDOM_HPP

#include <algorithm>
#include <array>
#include <cstdint>
#include <functional>
#include <iterator>
#include <limits>
#include <random>
#include <type_traits>

/*
 * This class serves as a wrapper around the mt19937 PRNG. It ensures proper
 * seeding without the need to carry extra lines of code and plop them into
 * every program.
 *
 * While a template class, Random can only assume unsigned integer types
 *
 * It is able to select either mt19937 or mt19937_64 based on T
 */
namespace rnd {
// Substitution Failure Is Not An Error (SFINAE). A longer description is at the bottom.
template <typename T = std::uint32_t, typename Enable = void>
class Mersenne;

template <typename T>
using AllowForUnsigned = std::enable_if_t<std::is_unsigned_v<T>>;

template <typename T>
class Mersenne<T, AllowForUnsigned<T>>
{
public:
    Mersenne();
    T operator()();

    using result_type = T; // aliases are preferred over typedefs as of C++11
    static constexpr result_type min();
    static constexpr result_type max();

private:
    using Twister = std::conditional_t<sizeof(T) <= 4, std::mt19937, std::mt19937_64>;
    Twister engine_;
};

// Mersenne class implementation
template <typename T>
Mersenne<T, AllowForUnsigned<T>>::Mersenne()
{
    // Proper seeding of mt19937 taken from:
    // https://kristerw.blogspot.com/2017/05/seeding-stdmt19937-random-number-engine.html
    // Body walkthrough at end of file
    std::random_device rd;
    std::array<T, Twister::state_size> seed_data;
    std::generate_n(std::begin(seed_data), seed_data.size(), std::ref(rd));
    std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
    engine_ = Twister(seq);
}

// These functions are required for rnd::Mersenne to work with distributions in <random>
template <typename T>
T
Mersenne<T, AllowForUnsigned<T>>::operator()()
{
    return engine_();
}

template <typename T>
constexpr typename Mersenne<T, AllowForUnsigned<T>>::result_type
Mersenne<T, AllowForUnsigned<T>>::min()
{
    return std::numeric_limits<T>::min();
}

template <typename T>
constexpr typename Mersenne<T, AllowForUnsigned<T>>::result_type
Mersenne<T, AllowForUnsigned<T>>::max()
{
    return std::numeric_limits<T>::max();
}
} // rnd

#endif

/* Long notes on the constructor
 * The constructor will be examined line by line, as it is the most crucial part of this class.
 *
 * - std::random_device rd;
 * Many times, you will see mt19937 seeded with just std::random_device. This code compiles, but is
 * simply not adequate. std::random_device returns a 32-bit value, but the state size of mt19937
 * and mt19937_64 is 19968 bits. Giving it only 32 bits to start with severely hampers the engine
 * by limiting its possible output streams to a tiny fraction of its capabilities.
 *
 * std::random_device is intended to utilize your hardware to get a truly random number if your
 * hardware supports it. If it doesn't, it will use a deterministic sequence. This is typically not
 * ideal at all, but we'll see that it ends up being okay.
 *
 * - std::array<T, Twister::state_size> seed_data;
 * This is just an array, but using the <array> library. The library itself just wraps the regular
 * array with some functions to make its use similar to other STL containers. Notably, std::array
 * provides iterators
 *
 * - std::generate_n(std::begin(seed_data), seed_data.size(), std::ref(rd));
 * From <algorithm>, this function takes a function object and executes it many times, placing the
 * results into a container. We can see this reading the arguments from left to right. I want the
 * data placed in my seed_data array, starting at the beginning. I want to fill the entire array,
 * so I pass the size of the array. Finally, the function I want to execute is my random_device.
 *
 * - std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
 * I then copy the data into a std::seed_seq. std::seed_seq comes from <random>, is the way to
 * properly create a robust starting state for PRNGs like mt19937. There are two main reasons for
 * needing the std::seed_seq object. The first is that the PRNGs in <random> were not designed to be
 * constructed with any container. When you need a sequence of values, they will only accept a
 * std::seed_seq, or another object that meets the requirements of a SeedSequence
 * (https://en.cppreference.com/w/cpp/named_req/SeedSequence)
 *
 * The second reason is explained with the next line of code.
 *
 * - engine_ = Twister(seq);
 * The PRNG constructor overload that accepts a std::seed_seq will also call the std::seed_seq's
 * generate() function. The generate() function will apply a transformation to all the elements in
 * the std::seed_seq. This is also why we don't care as much about whether std::random_device
 * executes non-deterministically or deterministically. Even with a poor starting state, generate()
 * helps ensure that the state is better off.
 *
 * By doing all this, we ensure that mt19937(_64) is seeded with a full state of randomly generated
 * data, which will lift the heavy limitations imposed on the engine when we seed with just a
 * single 32-bit value.
 */

/*
 * SFINAE
 *
 * SFINAE is a programming technique to control template instantiation. The name implies that if an
 * invalid substitution is made, the compiler will simply remove that overload, and not stop with a
 * compilation error. An added benefit is that editors can lint and catch bad substitutions for you.
 *
 * Each part of this use will be described.
 *
 * - template <typename T = std::uint32_t, typename Enable = void>
 * - class Mersenne;
 * This is the "catch-all" case. This class only works with unsigned integer types; any other type
 * will invoke this incomplete definition, and you'll get an error about an incomplete type. Note
 * that it's possible to designate a default typename. This was done for both T and Enable. The
 * default for Enable is required for SFINAE, the default for T allows programmers to use the C++17
 * template type deduction, i.e., it allows a declaration of rnd::Mersenne m; . Note that I am not
 * required to specify a template type or even provide empty <>. C++17 allows template types to be
 * deduced.
 *
 * - template <typename T>
 * - using AllowForUnsigned = std::enable_if_t<std::is_unsigned_v<T>>;
 * This is optional, but makes typing and reading of the code much easier. It's a simple alias. The
 * right hand side is what allows for much easier to write SFINAE. It's a type that will not
 * overload if T is not an unsigned integer type.
 *
 * - template <typename T>
 * - class Mersenne<T, AllowForUnsigned<T>>
 * Finally, the class definition only exists when type AllowForUnsinged<T> exists, otherwise the
 * default of void is used to instantiate the incomplete type. When declaring rnd::Mersenne objects,
 * you only need to provide a single template parameter.
 *
 * Examples:
 * - rnd::Mersenne m;
 * Valid, the default type of T is std::uint32_t, which activates the class.
 *
 * - rnd::Mersenne<int> m;
 * Invalid, int is not unsigned so the type AllowForUnsigned will not instantiate.
 *
 * - rnd::Mersenne<std::uint64_t> m;
 * Valid, T is std::uint64_t, which is an unsigned type, so AllowForUnsigned will overload and
 * things will work as expected.
 *
 * Note that none of the examples provide a second template parameter. AllowForUnsigned will exist
 * or not based on T, so all that is needed is T when writing code using the class.
 */