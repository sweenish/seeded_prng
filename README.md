# seeded_prng

C++11 introduced `<random>`, which is an excellent library for pseudo-random
number generation. And if you spend any time on StackOverflow looking at random
number generation, you'll see that the `std::mt19937` PRNG is very popular. It is
also very easy to improperly seed. 

This class acts as a wrapper to ensure that the PRNG is always seeded properly.
It will also automatically select either `std::mt19937` or `std::mt19937_64` 
depending on unsigned integer type that is selected.